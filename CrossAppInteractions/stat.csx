// 1) Install DotNet Core: https://dotnet.microsoft.com/download
// 2) Install dotnet script: dotnet tool install -g dotnet-script
// 3) Run the script: dotnet script stat.csx
//    or: dotnet script stat.csx -- -i apps-unique.json

#r "nuget: Newtonsoft.Json, 12.0.3"
#r "nuget: CommandLineParser, 2.7.82"
#load "utils.csx"

using CommandLine;

class Options
{
    [Option('i', "input", Required = false, Default = "apps-unique.json", HelpText = "Input file.")]
    public string Input { get; set; }
}

static void ShowMostPopularInteractionsFrom(int topN, HashSet<(Recipe, Recipe)> chains)
{
    Console.WriteLine($"Most popular interactions (top {topN}):");
    var topChains = chains
        .GroupBy(x => x.Item1)
        .OrderByDescending(x => x.Key.InstallationCount)
        .Take(topN);
    foreach (var group in topChains)
    {
        var fromRecipe = group.Key;
        var toRecipes = group.ToList();
        Console.WriteLine($"{fromRecipe.InstallationCount} {fromRecipe}: {toRecipes.Count} interactions");
        foreach (var groupByTrigger in toRecipes.GroupBy(x => x.Item2.ToTriggerId()))
        {
            Console.WriteLine($"    {groupByTrigger.Key}: {groupByTrigger.Count()} interactions");
        }
    }
}

static void ShowStat(int count, int topN, int randomK, List<Recipe> orderedRecipes, 
    Dictionary<(string, string), List<Trigger>> actionToTriggers)
{
    Console.WriteLine($"Sampling top {topN} + random {randomK}");
    var chainCountList = new List<int>(count);
    for (int i = 0; i < count; i++)
    {
        var (recipes, triggerToActionRecipes) = IndexRecipes(TopNRandomK(orderedRecipes, topN, randomK));

        //Dump(recipes);
        var chains = GenerateChains(recipes, actionToTriggers, triggerToActionRecipes);
        var interactCount = chains.Count;
        chainCountList.Add(interactCount);
    }

    Console.WriteLine($"    Mean={chainCountList.Average()}, Median={chainCountList.Median()}, StdDev={chainCountList.StdDev()}");
}

Parser.Default.ParseArguments<Options>(Args).WithParsed<Options>(o =>
{
    var (recipes, triggerToActionRecipes) = IndexRecipes(LoadRecipes(o.Input));
    var actionToTriggers = IndexActionTriggers();
    var chains = GenerateChains(recipes, actionToTriggers, triggerToActionRecipes);
    Console.WriteLine($"Total interactions: {chains.Count}");
    
    // TOP //
    const int TopN = 20;
    var topRecipes = recipes
        .OrderByDescending(x => x.InstallationCount)
        .Take(TopN)
        .ToDictionary(x => x.ToString(), x => new 
        { 
            InstallationCount = x.InstallationCount,
            To = new List<Recipe>(), 
            From = new List<Recipe>()
        });

    foreach(var c in chains)
    {
        if (topRecipes.TryGetValue(c.Item1.ToString(), out var fromInteractions))
        {
            fromInteractions.From.Add(c.Item2);
        }

        if (topRecipes.TryGetValue(c.Item2.ToString(), out var toInteractions))
        {
            toInteractions.To.Add(c.Item1);
        }
    }

    Console.WriteLine($"{"Apps", -110} | {"Users", -6} | {"To", -6} | {"From", -6} |");
    foreach(var app in topRecipes)
    {
        Console.WriteLine($"{app.Key, -110} | {app.Value.InstallationCount, 6} | {app.Value.To.Count, 6} | {app.Value.From.Count, 6} |");
    }

    Console.WriteLine();
    Console.WriteLine();
    Console.WriteLine("SAMPLING");
    const int SampleCount = 500;
    var orderedRecipesAll = recipes.OrderByDescending(x => ConvertCount(x)).ToList();

    ShowStat(SampleCount, 10, 10, orderedRecipesAll, actionToTriggers);
    ShowStat(SampleCount, 20, 10, orderedRecipesAll, actionToTriggers);
    ShowStat(SampleCount, 30, 10, orderedRecipesAll, actionToTriggers);
});

