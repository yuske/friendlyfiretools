using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;
using System.Globalization;

public static IEnumerable<TSource> DistinctBy<TSource, TKey>
    (this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
{
    HashSet<TKey> seenKeys = new HashSet<TKey>();
    foreach (TSource element in source)
    {
        if (seenKeys.Add(keySelector(element)))
        {
            yield return element;
        }
    }
}

public static double Median(this IEnumerable<int> values)
{
    var numbers = values.OrderBy(n=>n).ToList(); 
    var halfIndex = numbers.Count / 2; 

    if (numbers.Count % 2 == 0)
        return (numbers[halfIndex] + numbers[halfIndex - 1]) / 2.0;

    return numbers[halfIndex];
}

public static double StdDev(this IEnumerable<int> values)
{  
    var avg = values.Average();  
    var sum = values.Sum(d => Math.Pow(d - avg, 2));   
    return Math.Sqrt(sum / (values.Count() - 1));
}

public static HashSet<int> GetRandomNumbers(int min, int max, int count)
{
    var random = new Random();
    HashSet<int> randomNumbers = new HashSet<int>();

    for (int i = 0; i < count; i++) 
        while (!randomNumbers.Add(random.Next(min, max)));

    return randomNumbers;
}

static int ConvertCount(string value)
{
    var factor = 1;
    if (value[value.Length - 1] == 'k' || value[value.Length - 1] == 'K')
    {
        factor = 1000;
        value = value.Substring(0, value.Length - 1);
    }

    if (value.Contains(","))
    {
        throw new Exception($"Not parsed {value}");
    }

    return (int)(Double.Parse(value, NumberStyles.Any, CultureInfo.InvariantCulture) * factor);
}

static int ConvertCount(JObject value) =>
    ConvertCount((string)value["addCount"]);

class Recipe : JObject
{
    public Recipe(JObject obj) : base(obj) {}

    private int installationCount = -1;
    public int InstallationCount 
    {
        get
        {
            if (installationCount == -1)
            {
                installationCount = ConvertCount(this);
            }

            return installationCount;
        }
    }

    public string Url => (string)base["url"];
    //public string Title => (string)base["title"];

    public (string, string) ToTriggerId() =>
        ((string)base["triggerChannelTitle"], (string)base["triggerTitle"]);
    public (string, string) ToActionId() =>
        ((string)base["actionChannelTitle"], (string)base["actionTitle"]);
    public override string ToString() => $"{ToTriggerId().Item1}::{ToTriggerId().Item2} -> {ToActionId().Item1}::{ToActionId().Item2}";
}

class Action : JObject
{
    public Action(JObject obj) : base(obj) {}
    public (string, string) ToId() => ((string)base["actionChannelName"], (string)base["actionTitle"]);
    public override string ToString() => $"{ToId()}";
}

class Trigger : JObject
{
    public Trigger(JObject obj) : base(obj) {}
    public string ToChannelId() => (string)base["triggerChannelName"];
    public (string, string) ToId() => ((string)base["triggerChannelName"], (string)base["triggerTitle"]);
    public override string ToString() => $"{ToId()}";
}

static void CheckFile(string fileName)
{
    var info = new FileInfo(fileName);
    if (!info.Exists)
    {
        throw new Exception($"The file '{fileName}' does not exist. Run tools from 'CrossAppInteractions' directory");
    }

    if (info.Length < 150)
    {
        throw new Exception($"The file '{fileName}' is empty or too small. Use Git LFS to clone the repository with large files (see README for details)");
    }
}

Dictionary<int, List<T>> Index<T>(string fileName, Func<JObject, T> factory)
{
    CheckFile(fileName);
    var result = new Dictionary<int, List<T>>();
    var actionsJson = JArray.Parse(File.ReadAllText(fileName));
    foreach (var json in actionsJson)
    {
        var ids = json["chainId"];
        if (ids == null)
            continue;

        foreach (int id in ids)
        {
            if (result.TryGetValue(id, out var list))
            {
                list.Add(factory((JObject)json));
            }
            else
            {
                result.Add(id, new List<T>{factory((JObject) json)});
            }
        }
    }

    return result;
}

const int RecipesSize = 280000;

List<Recipe> LoadRecipes(string fileName)
{
    CheckFile(fileName);
    var recipes = new List<Recipe>(RecipesSize);
    foreach (var line in File.ReadLines(fileName))
    {
        var json = JObject.Parse(line);
        recipes.Add(new Recipe(json));
    }

    return recipes;
}

static (List<Recipe>, Dictionary<(string, string), List<Recipe>>) IndexRecipes(IEnumerable<Recipe> recipes)
{
    var recipesList = new List<Recipe>(RecipesSize);
    var triggerToActions = new Dictionary<(string, string), List<Recipe>>(RecipesSize);
    foreach (var recipe in recipes)
    {
        recipesList.Add(recipe);

        var triggerId = recipe.ToTriggerId();
        if (triggerToActions.TryGetValue(triggerId, out var list))
        {
            list.Add(recipe);
        }
        else
        {
            triggerToActions.Add(triggerId, new List<Recipe>{recipe});
        }
    }

    return (recipesList, triggerToActions);
}

Dictionary<(string, string), List<Trigger>> IndexActionTriggers()
{
    var actions = Index<Action>("classificationActionsFinal.json", x => new Action(x));
    var triggers = Index<Trigger>("classificationTriggersFinal.json", x => new Trigger(x));

    var actionToTriggers = new Dictionary<(string, string), List<Trigger>>();
    foreach (var actionPair in actions)
    {
        var id = actionPair.Key;
        foreach (var action in actionPair.Value)
        {
            var key = action.ToId();
            if (!actionToTriggers.TryGetValue(key, out var list))
            {
                list = new List<Trigger>();
                actionToTriggers.Add(key, list);
            }

            if (triggers.TryGetValue(id, out var sublist))
            {
                list.AddRange(sublist);
            }
        }
    }

    //Console.WriteLine($"actionToTriggers: {actionToTriggers.Count}");
    return actionToTriggers;
}

static IEnumerable<Recipe> TopNByInteractions(List<Recipe> recipesAll, 
    Dictionary<(string, string), List<Trigger>> actionToTriggers, int topN)
{
    var (recipes, triggerToActionRecipes) = IndexRecipes(recipesAll);
    var chains = GenerateChains(recipes, actionToTriggers, triggerToActionRecipes);
    var result = chains
        .OrderByDescending(chain => 
        {
            var (from, _) = chain;
            return from.InstallationCount;
        })
        .DistinctBy(chain => 
        {
            var (from, _) = chain;
            return from.InstallationCount;
        })
        .Take(topN)
        .Select(chain => 
        {
            var (from, _) = chain;
            return from;
        })
        .ToList();

    return result;
}

static IEnumerable<Recipe> TopNRandomK(List<Recipe> orderedRecipes, int topN, int randomK)
{
    foreach (var item in orderedRecipes.Take(topN))
    {
        yield return item;
    }

    var randomNumbers = GetRandomNumbers(0, orderedRecipes.Count - topN, randomK);
    int index = 0;
    foreach (var item in orderedRecipes.Skip(topN))
    {
        if (randomNumbers.Contains(index))
        {
            randomK--;
            yield return item;
        }

        if (randomK == 0)
        {
            break;
        }

        index++;
    }
}

static HashSet<(Recipe, Recipe)> GenerateChains(List<Recipe> recipes, 
    Dictionary<(string, string), List<Trigger>> actionToTriggers,
    Dictionary<(string, string), List<Recipe>> triggerToActionRecipes)
{
    var chains = new HashSet<(Recipe, Recipe)>();
    for (int i = 0; i < recipes.Count; i++)
    {
        var recipe = recipes[i];
        if (actionToTriggers.TryGetValue(recipe.ToActionId(), out var triggers))
        {
            foreach (var trigger in triggers)
            {
                if (triggerToActionRecipes.TryGetValue(trigger.ToId(), out var children))
                {
                    foreach (var child in children)
                    {
                        chains.Add((recipe, child));
                    }
                }
            }
        }
    }

    return chains;
} 

static void Dump(IEnumerable<(Recipe, Recipe)> chains)
{
    Console.WriteLine($"Dump chains: {chains.Count()}");
    foreach (var (from, to) in chains)
    {
        Console.WriteLine($"    {from} ---> {to}");
    }
}

static void Dump(List<Recipe> recipes)
{
    Console.WriteLine($"Dump recipes: {recipes.Count}");
    foreach (var recipe in recipes)
    {
        Console.WriteLine($"    {recipe}");
    }
}
