// 1) Install DotNet Core: https://dotnet.microsoft.com/download
// 2) Install dotnet script: dotnet tool install -g dotnet-script
// 3) Run the script: dotnet script convert.csx -- -i apps.json -o apps-unique.json

#r "nuget: Newtonsoft.Json, 12.0.3"
#r "nuget: CommandLineParser, 2.7.82"
#load "utils.csx"

using Newtonsoft.Json.Linq;
using CommandLine;

class Options
{
    [Option('i', "input", Required = true, HelpText = "Input file.")]
    public string Input { get; set; }

    [Option('o', "output", Required = true, HelpText = "Output file.")]
    public string Output { get; set; }
}

Parser.Default.ParseArguments<Options>(Args).WithParsed<Options>(o =>
{
    const int RecipesSize = 280000;
    var recipes = new Dictionary<(string, string, string, string), JObject>(RecipesSize);
    CheckFile(o.Input);
    foreach (var line in File.ReadLines(o.Input))
    {
        var json = JObject.Parse(line);
        var key = (
            (string)json["triggerChannelTitle"], 
            (string)json["triggerTitle"],
            (string)json["actionChannelTitle"],
            (string)json["actionTitle"]
        );

        if (recipes.TryGetValue(key, out var obj))
        {
            obj["addCount"] = (ConvertCount((string)obj["addCount"]) + ConvertCount((string)json["addCount"])).ToString();
        }
        else
        {
            recipes.Add(key, json);
        }
    }

    Console.WriteLine("Count: " + recipes.Count);
    File.WriteAllLines(o.Output, recipes.Values.Select(x => x.ToString(Newtonsoft.Json.Formatting.None)));
});

