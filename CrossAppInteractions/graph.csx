// 1) Install DotNet Core: https://dotnet.microsoft.com/download
// 2) Install dotnet script: dotnet tool install -g dotnet-script
// 3) Run the script: dotnet script graph.csx -- <list of app urls split by space>
//    e.g.: dotnet script graph.csx -- https://ifttt.com/applets/158p-instagram-a-flickr https://ifttt.com/applets/35255p-instagram-to-flickr https://ifttt.com/applets/1302p-flickr-photos-to-fb https://ifttt.com/applets/367p-flickr2dropbox https://ifttt.com/applets/39181p-instagram-to-facebook https://ifttt.com/applets/31923p-instagram https://ifttt.com/applets/63830p-facebook-flickr

#r "nuget: Newtonsoft.Json, 12.0.3"
#load "utils.csx"

using Newtonsoft.Json.Linq;
using System.Text.RegularExpressions;

class GraphVizBuilder
{
    private readonly StringBuilder writer;
    private readonly Dictionary<string, ulong> triggers = new Dictionary<string, ulong>(300000);
    private readonly Dictionary<string, ulong> actions = new Dictionary<string, ulong>(300000);
    private readonly HashSet<(ulong, ulong)> edges = new HashSet<(ulong, ulong)>();
    private ulong nodeId;

    public GraphVizBuilder()
    {
        writer = new StringBuilder();
        writer.AppendLine("digraph G {");
        writer.AppendLine("node [fontsize = 14];");
        writer.AppendLine("newrank=true;");
    }
    
    public void AddRecipe((string, string) trigger, (string, string) action)
    {
        var from = AddTrigger($"{trigger.Item1}:{trigger.Item2}");
        var to = AddAction($"{action.Item1}:{action.Item2}");
        AddEdge(from, to, "recipe");
    }

    public void AddManualMatching((string, string) action, (string, string) trigger)
    {
        var from = AddAction($"{action.Item1}:{action.Item2}");
        var to = AddTrigger($"{trigger.Item1}:{trigger.Item2}");
        AddEdge(from, to, "match");
    }

    public void Save(string fileName)
    {
        var dotFile = fileName + ".gv";
        using (var writer2 = new StreamWriter(dotFile))
        {
            writer2.WriteLine(writer);
            writer2.WriteLine("}");
        }
    }
    
    public void GenerateImage(string fileName)
    {
        var dotFile = fileName + ".gv";
        var pngFile = fileName + ".png"; 
        var process = Process.Start(new ProcessStartInfo
        {
            FileName = "dot",
            Arguments = "\"" + dotFile + "\" -Tpng -o \"" + pngFile + "\"",
            UseShellExecute = false,
            CreateNoWindow = true,
            RedirectStandardOutput = true
        });

        process?.WaitForExit();
    }

    private ulong AddAction(string name)
    {
        if (actions.TryGetValue(name, out var id))
            return id;

        nodeId++;
        actions.Add(name, nodeId);

        writer.Append(nodeId);
        writer.Append(" [");
        var isFirst = true;
        WriteAttribute("label", Escape(name), ref isFirst);
        WriteAttribute("shape", "box", ref isFirst);
        WriteAttribute("color", "dodgerblue3", ref isFirst);
        writer.AppendLine("];");
        return nodeId;
    }

    private ulong AddTrigger(string name)
    {
        if (triggers.TryGetValue(name, out var id))
            return id;

        nodeId++;
        triggers.Add(name, nodeId);

        writer.Append(nodeId);
        writer.Append(" [");
        var isFirst = true;
        WriteAttribute("label", Escape(name), ref isFirst);
        WriteAttribute("shape", "box", ref isFirst);
        WriteAttribute("color", "indianred3", ref isFirst);
        writer.AppendLine("];");
        return nodeId;
    }

    private void AddEdge(ulong from, ulong to, string label)
    {
        if (!edges.Add((from, to)))
            return;

        writer.Append(from);
        writer.Append(" -> ");
        writer.Append(to);
        writer.Append(" [");
        var isFirst = true;
        switch (label)
        {
            case "recipe":
                WriteAttribute("color", "indianred3", ref isFirst);
                break;
            case "match":
                WriteAttribute("color", "dodgerblue3", ref isFirst);
                break;
            default:
                WriteAttribute("color", "gray40", ref isFirst);
                WriteAttribute("label", label, ref isFirst);
                break;
        }
        
        writer.AppendLine("];");
    }
    
    private void WriteAttribute(string name, string value, ref bool isFirst)
    {
        if (String.IsNullOrEmpty(value)) 
            return;

        if (isFirst)
            isFirst = false;
        else
            writer.Append(", ");

        writer.AppendFormat("{0}=\"{1}\"", name, value);
        //writer.AppendFormat("{0}=\"{1}\"", name, Escape(value));
    }

    private static string Escape(string text)
    {
        if (Regex.IsMatch(text, @"^[\w\d]+$"))
        {
            return text;
        }

        return text.Replace("\\", "\\\\").Replace("\r", "").Replace("\n", "\\n").Replace("\"", "\\\"");
    }
}

static void SaveGraph((List<Recipe>, Dictionary<(string, string), List<Recipe>>) indexRecipes, 
    string fileName, Dictionary<(string, string), List<Trigger>> actionToTriggers)
{
    var (recipes, triggerToActionRecipes) = indexRecipes;

    var builder = new GraphVizBuilder();
    //for (int i = 0; i < Math.Min(recipes.Count, 100); i++)
    for (int i = 0; i < recipes.Count; i++)
    {
        var recipe = recipes[i];
        var triggerRecipe = recipe.ToTriggerId();
        var actionRecipe = recipe.ToActionId();
        builder.AddRecipe(triggerRecipe, actionRecipe);

        if (actionToTriggers.TryGetValue(actionRecipe, out var listTriggers))
        {
            foreach (var trigger in listTriggers)
            {
                var triggerNew = trigger.ToId();
                if (triggerToActionRecipes.ContainsKey(triggerNew))
                {
                    builder.AddManualMatching(actionRecipe, triggerNew);
                }
            }
        }
    }

    builder.Save(fileName);
    builder.GenerateImage(fileName);
}

void Validate(List<Recipe> recipes)
{
    foreach(var arg in Args)
    {
        if (!recipes.Any(x => x.Url == arg))
            Console.WriteLine($"The applet '{arg}' not found in the knowledge base.");
    }
}


if (Args.Count == 0)
{
    Console.WriteLine("Required list of applets is missing.");
    return;
}

var recipes = LoadRecipes("apps.json").Where(x => Args.Contains(x.Url)).ToList();
Validate(recipes);
SaveGraph(IndexRecipes(recipes), "graph", IndexActionTriggers());
