# Friendly Fire: Cross-App Interactions in IoT Platforms - Tools #

### Safety of cross-app interactions ###

We provide [a dataset](CrossAppInteractions/apps.json) of IFTTT apps and the tools to analyse the dataset:

* `stat.csx` - showing information about interactions between different apps
* `graph.csx` - showing a graph of app interactions
* `convert.csx` - converting the full dataset of IFTTT apps to a dataset of unique apps

The tools are developed in C# and .NET Core. Follow the next steps to set up the environment:

* We use Git LFS to store JSON files of our knowledge base and dataset. Please install Git LFS using [the installation guide](https://github.com/git-lfs/git-lfs/wiki/Installation) and clone the repository again, or download all JSON files manually
* Download and install [.NET Core](https://dotnet.microsoft.com/download)
* Install `dotnet-script` using the .NET CLI command: `dotnet tool install -g dotnet-script`
* Download and install [Graphviz package](https://graphviz.org/download/) (only necessary for `graph.csx`)
* Open a terminal in the directory `CrossAppInteractions` to executute the tools

We also provide [a dataset](CrossAppInteractions/apps-unique.json) of unique IFTTT apps that created based on the full dataset. However, you can build the same dataset using the command:
```
dotnet script convert.csx -- -i apps.json -o apps-unique.json
```

Execute the script `stat.csx` to get information about interactions between the most popular apps:
```
dotnet script stat.csx -- -i apps-unique.json
```

Execute the script `graph.csx` to build a graph of interactions of the selected apps:
```
dotnet script graph.csx -- <a list of app URLs split by space>
```

The selected apps must exist in [our dataset](CrossAppInteractions/apps.json). The script identifies them by a unique URL. For example:
```
dotnet script graph.csx -- https://ifttt.com/applets/158p-instagram-a-flickr https://ifttt.com/applets/35255p-instagram-to-flickr https://ifttt.com/applets/1302p-flickr-photos-to-fb https://ifttt.com/applets/367p-flickr2dropbox https://ifttt.com/applets/39181p-instagram-to-facebook https://ifttt.com/applets/31923p-instagram https://ifttt.com/applets/63830p-facebook-flickr
```

### Security type system ###

We provide two implementation of the type system enforcing non-interference in IoT apps:

* `caitappTS.jar` - standard version
* `caitappTSd.jar` - version with declassification

The tools are developed in Java 11 and the `.jar` files contain all the necessary libraries, hence it is sufficient a Java Runtime Environment to execute them.

```
Usage: java -jar caitappTS.jar apps_file_path/apps_file_name.app
Usage: java -jar caitappTSd.jar apps_file_path/apps_file_name.app
```

`apps_file_name.app` should contain the code of an app or a set of apps

This file is structured as follows:
```
appName[service<mode> ; ... ; service<mode> # fix X . { listen(service ; ... ; service) ; app_payload ; X }] :: appName[service<mode> ; ... ; service<mode> # fix X . { listen(service ; ... ; service) ; app_payload ; X }]
service:level ; ... ; service:level
```

The first line is the code of one or more apps. In services declaration, mode could be `R`, `W` or `RW`. A listen contruct is mandatory, and `app_payload` should contain the actual code of the app. The double pipes `::` represent the parallel composition, when we have more than one app.

The code may contain declassified expressions, of the form `declassify(e,L)`, when `caitappTSd.jar` is used.

The second line is the security typing environment, assigning security levels (`L` or `H`) to all services declared in the apps.

Some examples.

```
eSaver[none<R> ; timeH<R> ; lights<RW> # fix X . { listen(none; lights) ; none := read(none) ; if (8 < read(timeH) && read(timeH) < 18) then { if (none >= 5 && lights = "on") then { lights := "off" ; update(lights) } else {skip} } else { skip } ; X }]
none:H; timeH:L; lights:L
```
```
DbLogger[dropbox<R> ; timeH<R> ; timeM<R> ; timeS<R> ; dblog<W> # fix X . { listen(dropbox) ; dropbox := read(dropbox) ; dblog := dblog + timeH + timeM + timeS + declassify(dropbox,L) ; update(dblog) ; X }]
dropbox:H;timeH:L;timeM:L;timeS:L;dblog:L
```

